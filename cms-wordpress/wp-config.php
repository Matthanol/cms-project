<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oana-wordpress');

/** MySQL database username */
define('DB_USER', 'wp-dbu');

/** MySQL database password */
define('DB_PASSWORD', 'wpu-pw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+)B4_TH}$**l-UY/)y1d/~ =/x%u?9n`ohOFS{,[ 1/K2g]`d.$Hshsu>cRT;kk:');
define('SECURE_AUTH_KEY',  '[[p7ADQ!#*ELAH#E,gqs@p>xj|L`E?X%W!{;gDE|bPGb]^vRa}bbhM|V=%.VT9jV');
define('LOGGED_IN_KEY',    'ugX/h>dd(V9lc~T3+u!Q60DW=&OJUxk{Xi{TX%Q46H: DQlO`j12i}yG Aat+pw,');
define('NONCE_KEY',        'Y*c&}vp^r3o|/CR:s>q+W4yYb`&zqBH14]iW%Uy-!BlE |Q}(PsWiUmgmmxhM:=+');
define('AUTH_SALT',        'H&~4Xsq}.]9bwMK}Z8s0A1Rj1hDL ,p1C.3gi8w34%aFX#nU0bBsTui ?e?uwERV');
define('SECURE_AUTH_SALT', 'z6EH9wt2XW&uf}HqE,/G7bC)<!WAXyq8DG]A0yQ`,uAa6L=v(Aq4u{fcYW8!XG^P');
define('LOGGED_IN_SALT',   ')F#3#+$Nozf+^?-)i5~jzXCdA/JSW?/-GShpJfu^yr1C(Di,%jjCi}]>:j3O/-pK');
define('NONCE_SALT',       'K<z`Q8u@*J:BHE#9HuEo>r:u(.+D(,0RnEU<7v;&T}vOLxd^2lh)$0Vjow|%aNjD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
